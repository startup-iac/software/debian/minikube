#!/bin/bash

# Install Required
sudo apt-get install -y apt-transport-https ca-certificates curl

# Add Kubectl Repository
wget -qO - https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o $KEYRINGS_PATH/kubernetes-keyring.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRINGS_PATH/kubernetes-keyring.gpg] \
    https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /" \
    | sudo tee /etc/apt/sources.list.d/kubernetes.list

# Update Repositories
sudo apt-get update

# Install Kubectl
sudo apt-get install -y kubectl

# Install Minikube
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 -O /tmp/minikube-linux-amd64
sudo install /tmp/minikube-linux-amd64 /usr/local/bin/minikube